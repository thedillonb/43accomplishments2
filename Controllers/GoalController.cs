//
//  GoalController.cs
//
//  Author:
//       dillonb <>
//
//  Copyright (c) 2012 dillonb
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using MonoTouch.Dialog;
using MonoTouch.UIKit;
using Accomplishments.Data;
using System.Xml.XPath;
using MonoTouch.Foundation;

namespace Accomplishments.Controllers
{
    public class GoalController : DialogViewController
    {
        public GoalController()
            : base(UITableViewStyle.Plain, null, false)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            Reload();
        }

        private void Reload()
        {
            var resp = Application.Request("get_person", new Tuple<string, string>("id", "thedillonb"));
            var elements = resp.XPathSelectElements("//open_goals/goal");
            var section = new Section();

            foreach (var e in elements)
            {
                var el = new Item(e.XPathSelectElement("name").Value);
                section.Add(el);
            }

            var root = new RootElement(Title) { section };
            Root = root;
        }


        public override DialogViewController.Source CreateSizingSource (bool unevenRows)
        {
            return new Source(this);
        }

        private new class Source : DialogViewController.Source
        {
            public Source(GoalController dvc)
                : base(dvc)
            {
            }
        }

        private class Item : StyledStringElement
        {
            public Item(string caption)
                : base(caption)
            {
            }

            public override void WillDisplay (UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
            {
                base.WillDisplay(tableView, cell, indexPath);
                cell.BackgroundColor = UIColor.FromRGB(0xD6, 0xF7, 0x88);

            }
        }
    }
}

