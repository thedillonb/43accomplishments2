//
//  Application.cs
//
//  Author:
//       dillonb <>
//
//  Copyright (c) 2012 dillonb
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using RestSharp;
using System.Text;
using System.Xml.Linq;

namespace Accomplishments.Data
{
    public static class Application
    {
        private static RestClient _client = new RestClient("http://www.43things.com/service/");
        private static readonly string ApiKey = "3675982@43USNYbTzQn7Y";
        private static readonly string Username = "thedillonb";
        private static readonly string Password = "djames";

        public static XElement Request(string request, params Tuple<string, string>[] parameters)
        {
            var url = new StringBuilder();
            url.Append(request);
            url.AppendFormat("?api_key={0}", ApiKey);
            url.AppendFormat("&username={0}", Username);
            url.AppendFormat("&password={0}", Password);

            foreach (var p in parameters)
                url.AppendFormat("&{0}={1}", p.Item1, p.Item2);

            var resp = _client.Execute(new RestRequest(url.ToString(),Method.GET));
            return XElement.Parse(resp.Content);
        }
    }
}

